from flask import Flask, render_template, redirect, url_for, request
from flask_frozen import Freezer

app = Flask(__name__)
app.config.from_mapping({
    "FREEZER_BASE_URL": "https://zen.danjou.dev",
    "FREEZER_IGNORE_MIMETYPE_WARNINGS": True,
    "IMAGE_WIDTH": 344,
    "IMAGE_HEIGHT": 180,
    "TITLE": "Zen of Coding",
    "DESCRIPTION": "This is the Zen of Python, ney, Coding!"
})
freezer = Freezer(app)


@app.cli.command("freeze")
def freeze():
    freezer.freeze()


@freezer.register_generator
def index():
    yield 'number', {'nr': 0}


ZENS = [
    'Zen of <span class="strike">Python</span> <span class="italic">Coding</span>',
    'Beautiful is better than ugly.',
    'Explicit is better than implicit.',
    'Simple is better than complex.',
    'Complex is better than complicated.',
    'Flat is better than nested.',
    'Sparse is better than dense.',
    'Readability counts.',
    'Special cases aren\'t special enough to break the rules.',
    'Although practicality beats purity.',
    'Errors should never pass silently.',
    'Unless explicitly silenced.',
    'In the face of ambiguity, refuse the temptation to guess.',
    'There should be one (and preferably only one) obvious way to do it.',
    'Although that way may not be obvious at first unless you\'re Dutch.',
    'Now is better than never.',
    'Although never is often better than <span class="italic">right</span> now.',
    'If the implementation is hard to explain, it\'s a bad idea.',
    'If the implementation is easy to explain, it may be a good idea.',
    'Namespaces are one honking great idea — let\'s do more of those!'
]


@app.route("/", defaults={'nr': 0})
@app.route("/<int:nr>/")
def number(nr: int):
    if nr not in range(len(ZENS)):
        return redirect(url_for(request.endpoint), code=301)
    return render_template('number.html', nr=nr, zens=ZENS)


@app.route("/image.png", defaults={'nr': 0}, endpoint="image.png")
@app.route("/image.html", defaults={'nr': 0}, endpoint="image.html")
@app.route("/<int:nr>/image.png", endpoint="image.png")
@app.route("/<int:nr>/image.html", endpoint="image.html")
def image(nr: int):
    if nr not in range(len(ZENS)):
        return redirect(url_for(request.endpoint), code=301)
    return render_template('image.html', nr=nr, zens=ZENS)
