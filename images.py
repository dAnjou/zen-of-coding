import asyncio
import os
from pathlib import Path

import click
from pyppeteer import launch


async def run_pyppeteer(host: str, width: int, height: int, build_directory: Path):
    browser = await launch({"defaultViewport": {"width": width, "height": height}})
    page = await browser.newPage()
    for root, _, files in os.walk(build_directory):
        for f in files:
            if f != "image.html":
                continue
            click.echo(image_file := Path(root) / f)
            png = image_file.parent / (image_file.stem + ".png")
            html = "/".join([host, str(image_file.relative_to(image_file.parts[0]))])
            click.echo(f'Saving image of {html} to {png}')
            await page.goto(html)
            await page.screenshot({'path': f"{png}"})
            image_file.unlink()
    await browser.close()


@click.command(context_settings=dict(ignore_unknown_options=True))
@click.argument("host")
@click.argument("width", type=click.INT)
@click.argument("height", type=click.INT)
@click.argument("build_directory", type=click.Path(exists=True, file_okay=False, path_type=Path))
def cli(host: str, width: int, height: int, build_directory: Path) -> None:
    asyncio.get_event_loop().run_until_complete(run_pyppeteer(host, width, height, build_directory))


if __name__ == '__main__':
    cli()
