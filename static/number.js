// BEGIN - FOR BACKWARDS COMPATIBILITY
const match = location.hash.match(/#(?<nr>\d+)/);
if (match) {
  const nr = match.groups["nr"] || "";
  if (nr) {
    window.location.replace(window.location.origin + "/" + nr + "/");
  }
}
// END - FOR BACKWARDS COMPATIBILITY

const nr = location.pathname.replaceAll('/', '').trim();
document.getElementById(nr)?.scrollIntoView();

tippy('h1 a', {
  content: 'Clear selection',
  placement: 'left',
});

tippy('#zens a', {
  content: 'Copy',
  placement: 'left',
  onShown(instance) {
    setTimeout(instance => instance.hide(), 2000, instance)
  },
  onHidden(instance) {
    instance.setContent('Copy')
  }
});

const clipboard = new ClipboardJS('#zens a', {
  text: trigger => {
    return '"' + trigger.innerText + '" ' + document.location.origin + trigger.getAttribute('href');
  }
});

clipboard.on('success', e => {
  e.trigger._tippy.setContent('Copied!')
  e.trigger._tippy.show()
});
