setup:
	python3 -m venv venv
	./venv/bin/pip install -UI pip
	./venv/bin/pip install -r requirements.txt

freeze: setup
	rm -rf build
	./venv/bin/flask --app pages:app freeze
	./venv/bin/python -m http.server 8000 --directory build & \
		export SERVER_PID=$$!; \
		./venv/bin/python images.py "http://localhost:8000" 344 180 build; \
		kill $${SERVER_PID}
	rsync -r build/ public

debug: setup
	./venv/bin/flask --app pages:app --debug run --extra-files templates:static --port 8000

run: freeze
	@echo http://localhost:8000
	./venv/bin/python -m http.server 8000 --directory public
